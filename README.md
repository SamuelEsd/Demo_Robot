# Simulador robocup

## Requisitos previos
Tener instalado Ros en su versión noetic, además de una versión de python 3 inferior a la 3.9, de preferencia la 3.8.

Instalar paquetes necesarios para la simulación de la kobuki (robot que será simulado).

Para hacer esto se deben de seguir los siguientes pasos:

- Crear un nuevo workspace para dichos paquetes.

- Instalar los paquetes necesarios para la simulación de la kobuki.

- Modificar los paquetes para que funcionen con la version de Ros que usaremos.

- Compilar los paquetes de kobuki.

### Crear un nuevo workspace para dichos paquetes.
Comenzamos abriendo una nueva terminal de linux en el lugar donde colocaremos nuestros worspaces
de Ros y ejecutando

```
$ source /opt/ros/noetic/setup.bash

$ mkdir -p [kobuki_ws]/src

$ cd [kobuki_ws]/

$ catkin_make
```


  Donde [kobuki_ws] es el nombre que se le quiere dar al workspace donde se guardará la kobuki.

### Modificar los paquetes para que funcionen con la version de Ros que usaremos.
Una vez creado nuestro workspace clonamos los repositorios necesarios en el src del workspace.


  ```
$ cd src/

  $ git clone https://github.com/yujinrobot/yocs_msgs.git

  $ git clone https://github.com/yujinrobot/yujin_ocs.git

  $ git clone https://github.com/yujinrobot/kobuki.git

  $ git clone https://github.com/yujinrobot/kobuki_soft.git
```

  
### Instalar los paquetes necesarios para la simulación de la kobuki.

Dado que estos paquetes fueron originalmente creados para la versión anterior a noetic, melodic, debemos de aplicar los siguientes cambios:

- Entrar a yujin_ocs y borrar los directorios que inician con 'yocs_ar',
  sirven para leer códigos ar, pero no los vamos a usar de momento. No 
  eliminarlos causará errores al compilar.

- En la línea 5 del archivo kobuki_soft/kobuki_softnode/lauch/full.launch,
  cambiar "\$(find xacro)/xacro.py" por "\$(find xacro)/xacro".

- En el archivo kobuki/kobuki\_description/urdf/kokuki.urdf.xacro asegurarse que al final esté la línea con
  el prefijo xacro:

  `<xacro:kobuki_sim/>`

- En el archivo kobuki_standalone.urdf.xacro asegurarse que <kobuki/> 
     también tenga el prefijo xacro, como en el punto anterior.




### Compilar los paquetes de kobuki.
Ahora solo hace falta compilar para que todo lo referente a la kobuki esté listo.
  ```
  $ cd ..
  
  $ catkin_make
```


## Correr simulación 

Para comenzar necesitamos crear un espacio de trabajo de ROS donde alojaremos el paquete, así que procedemos a crearlo.
Los pasos son exactamente los mismos que en la sección de la instalación de paquetes kobuki, a diferencia que 
utilizaremos el nombre simulacion_ws para el nuevo paquete (se le puede cambiar el nombre por el que se prefera):

  ```
$ source /opt/ros/noetic/setup.bash
  $ mkdir -p [simulation_ws]/src
  $ cd [simulacion_ws]/
  $ source <path_to_kobuki_workspace>/devel/setup.bash
  $ catkin_make
```
El <path_to_kobuki_workspace> debe ser la ruta al workspace creado en los requisitos.

Si ambos workspaces estan en la misma carpeta, lo cual es lo recomendable, la ruta sería "source ../[kobuki_ws]/devel/setup.bash".

Si se hace esto al compilar deberá aparecer, entre los mensajes de compilación, la línea: 

    -- This workspace overlays: 
    <path\_to\_kobuki\_workspace>/devel;/opt/ros/noetic.


### Workspace creado
Hasta este punto ya tenemos el espacio de trabajo en donde alojaremos el paquete de ROS que necesitamos, por lo tanto, procedemos a descargar el programa desde este mismo repositorio.

Ahora clonamos los repositorios necesarios en src.


```
$ cd src/
$ git clone -b noetic git@gitlab.com:SamuelEsd/Demo_Robot.git
```

Nótese que estamos utilizando la rama noetic, si se intenta correr el programa con la versión de master no funconará.

Después de eso nos movemos a la carpeta principal del workspace y compilamos el proyecto.

```
$ cd ../..
$ catkin_make
$ source devel/setup.bash
```


### Correr un ejemplo del prgrama.

Ahora ya podemos correr el proyecto, con el siguiente comandon podemos ejecutar rviz con multiples tortugas:

`$ roslaunch hola_tortuga inicia_tortugas.launch`


Con esto se debe iniciar una ventana de Rviz mostrando 6 kobukis en distintas posiciones.
